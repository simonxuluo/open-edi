/**
* @file sdc.cpp
* @date 2020-11-25
* @brief
*
* Copyright (C) 2020 NIIC EDA
*
* All rights reserved.
*
* This software may be modified and distributed under the terms
*
* of the BSD license.  See the LICENSE file for details.
*/

#include "db/timing/sdc/sdc.h"
#include "db/timing/timinglib/analysis_view.h"
#include "db/timing/timinglib/analysis_corner.h"
#include "db/timing/timinglib/analysis_mode.h"

namespace open_edi {
namespace db {

AnalysisCorner* Sdc::getAnalysisCorner() const {
    AnalysisView *view = this->getAnalysisView();
    if (!view) {
        return nullptr;
    }
    return view->getAnalysisCorner();
}

AnalysisMode* Sdc::getAnalysisMode() const {
    AnalysisView *view = this->getAnalysisView();
    if (!view) {
        return nullptr;
    }
    return view->getAnalysisMode();
}

std::ostream &operator<<(std::ostream &os, Sdc &rhs) {
    rhs.print<SdcHierarchySeparatorContainerPtr>(os, rhs.getHierarchySeparatorContainer());
    rhs.print<SdcUnitsContainerPtr>(os, rhs.getUnitsContainer());

    rhs.print<SdcClockContainerPtr>(os, rhs.getClockContainer());
    rhs.print<SdcGroupPathContainerPtr>(os, rhs.getGroupPathContainer());
    rhs.print<SdcClockGatingCheckContainerPtr>(os, rhs.getClockGatingCheckContainer());
    rhs.print<SdcClockGroupsContainerPtr>(os, rhs.getClockGroupsContainer());
    rhs.print<SdcClockLatencyContainerPtr>(os, rhs.getClockLatencyContainer());
    rhs.print<SdcSenseContainerPtr>(os, rhs.getSenseContainer());
    rhs.print<SdcClockTransitionContainerPtr>(os, rhs.getClockTransitionContainer());
    rhs.print<SdcClockUncertaintyContainerPtr>(os, rhs.getClockUncertaintyContainer());
    rhs.print<SdcDataCheckContainerPtr>(os, rhs.getDataCheckContainer());
    rhs.print<SdcDisableTimingContainerPtr>(os, rhs.getDisableTimingContainer());
    rhs.print<SdcFalsePathContainerPtr>(os, rhs.getFalsePathContainer());
    rhs.print<SdcIdealLatencyContainerPtr>(os, rhs.getIdealLatencyContainer());
    rhs.print<SdcIdealNetworkContainerPtr>(os, rhs.getIdealNetworkContainer());
    rhs.print<SdcIdealTransitionContainerPtr>(os, rhs.getIdealTransitionContainer());
    rhs.print<SdcInputDelayContainerPtr>(os, rhs.getInputDelayContainer());
    rhs.print<SdcMaxDelayContainerPtr>(os, rhs.getMaxDelayContainer());
    rhs.print<SdcMaxTimeBorrowContainerPtr>(os, rhs.getMaxTimeBorrowContainer());
    rhs.print<SdcMinDelayContainerPtr>(os, rhs.getMinDelayContainer());
    rhs.print<SdcMinPulseWidthContainerPtr>(os, rhs.getMinPulseWidthContainer());
    rhs.print<SdcMulticyclePathContainerPtr>(os, rhs.getMulticyclePathContainer());
    rhs.print<SdcOutputDelayContainerPtr>(os, rhs.getOutputDelayContainer());
    rhs.print<SdcPropagatedClockContainerPtr>(os, rhs.getPropagatedClockContainer());

    rhs.print<SdcCaseAnalysisContainerPtr>(os, rhs.getCaseAnalysisContainer());
    rhs.print<SdcDriveContainerPtr>(os, rhs.getDriveContainer());
    rhs.print<SdcDrivingCellContainerPtr>(os, rhs.getDrivingCellContainer());
    rhs.print<SdcFanoutLoadContainerPtr>(os, rhs.getFanoutLoadContainer());
    rhs.print<SdcInputTransitionContainerPtr>(os, rhs.getInputTransitionContainer());
    rhs.print<SdcLoadContainerPtr>(os, rhs.getLoadContainer());
    rhs.print<SdcLogicContainerPtr>(os, rhs.getLogicContainer());
    rhs.print<SdcMaxAreaContainerPtr>(os, rhs.getMaxAreaContainer());
    rhs.print<SdcMaxCapacitanceContainerPtr>(os, rhs.getMaxCapacitanceContainer());
    rhs.print<SdcMaxFanoutContainerPtr>(os, rhs.getMaxFanoutContainer());
    rhs.print<SdcMaxTransitionContainerPtr>(os, rhs.getMaxTransitionContainer());
    rhs.print<SdcMinCapacitanceContainerPtr>(os, rhs.getMinCapacitanceContainer());
    rhs.print<SdcOperatingConditionsContainerPtr>(os, rhs.getOperatingConditionsContainer());
    rhs.print<SdcPortFanoutNumberContainerPtr>(os, rhs.getPortFanoutNumberContainer());
    rhs.print<SdcResistanceContainerPtr>(os, rhs.getResistanceContainer());
    rhs.print<SdcTimingDerateContainerPtr>(os, rhs.getTimingDerateContainer());
    rhs.print<SdcVoltageContainerPtr>(os, rhs.getVoltageContainer());
    rhs.print<SdcWireLoadMinBlockSizeContainerPtr>(os, rhs.getWireLoadMinBlockSizeContainer());
    rhs.print<SdcWireLoadModeContainerPtr>(os, rhs.getWireLoadModeContainer());
    rhs.print<SdcWireLoadModelContainerPtr>(os, rhs.getWireLoadModelContainer());
    rhs.print<SdcWireLoadSelectionGroupContainerPtr>(os, rhs.getWireLoadSelectionGroupContainer());

    rhs.print<SdcVoltageAreaContainerPtr>(os, rhs.getVoltageAreaContainer());;
    rhs.print<SdcLevelShifterStrategyContainerPtr>(os, rhs.getLevelShifterStrategyContainer());
    rhs.print<SdcLevelShifterThresholdContainerPtr>(os, rhs.getLevelShifterThresholdContainer());
    rhs.print<SdcMaxDynamicPowerContainerPtr>(os, rhs.getMaxDynamicPowerContainer());

    return os;
}

SdcPtr getSdc(ObjectId view_id) {
    AnalysisView *view = Object::addr<AnalysisView>(view_id);
    if (!view) {
        message->issueMsg("SDC", 28, open_edi::util::kError, view_id);
        return nullptr;
    }
    return view->getSdc();
}

SdcPtr getSdc(const std::string &view_name) {
    const auto &timing_lib = getTimingLib();
    AnalysisView *view = timing_lib->getAnalysisView(view_name);
    if (!view) {
        message->issueMsg("SDC", 6, open_edi::util::kError, view_name.c_str());
        return nullptr;
    }
    return view->getSdc();
}

}
}
