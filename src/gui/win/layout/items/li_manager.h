#ifndef EDI_GUI_LI_MANAGER_H_
#define EDI_GUI_LI_MANAGER_H_

#include <QList>
#include <QMap>
#include <QPoint>
#include <QString>

#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/core/db_init.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"

namespace open_edi {
namespace gui {

#define LI_MANAGER (LI_Manager::getInstance())

class LI_Base;
class LI_Layer;

class LI_Manager {
  public:
    ~LI_Manager() = delete;
    static LI_Manager* getInstance() {
        if (!inst_) {
            inst_ = new LI_Manager;
        }
        return inst_;
    }

    void            preDrawAllItems();
    QList<LI_Base*> getLiList();

    template <typename T>
    auto createLI(T&& li) {
        li_list_.append(li);
        li_map_[li->getName()] = li;
    };
    void              setLiVisibleByName(QString name, bool v);
    void              setLiBrushStyleByName(QString name, Qt::BrushStyle brush_style);
    LI_Base*          getLiByName(QString name);
    void              addLayer(open_edi::db::Layer* layer);
    void              setLayerVisibleByName(QString name, bool v);
    void              setLayerColorByName(QString name, QColor color);
    LI_Base*          getLayerByName(QString name);
    QList<LI_Layer*>  getLayerList();
    void              setSelectedArea(QPointF p1, QPointF p2);
    void              setSelectedArea(int x, int y, int w, int h);
    void              setSelectedArea(open_edi::db::Box area);
    open_edi::db::Box getSelectedAreaBox() { return selected_box_; };
    void              setHighlightSelectionArea(open_edi::db::Box area);
    int               getAnchorLLX() { return selected_box_.getLLX(); };
    int               getAnchorLLY() { return selected_box_.getLLY(); };
    void              resetSelectedBox();
    LI_Base*          getHighlightLI();
    void              addRoutingLayer(LI_Layer* layer) { routing_layer_list_.append(layer); };
    QList<LI_Layer*>& getRoutingLayersList() { return routing_layer_list_; };

  private:
    LI_Manager();
    static LI_Manager*      inst_;
    QList<LI_Base*>         li_list_;
    QMap<QString, LI_Base*> li_map_;
    int*                    scale_factor_;
    int                     llx_{0};
    int                     lly_{0};
    open_edi::db::Box       selected_box_;
    QList<LI_Layer*>        routing_layer_list_;
};
} // namespace gui
} // namespace open_edi

#endif