#include <QDebug>
#include <QThreadPool>

#include "../layout_def.h"
#include "li_base.h"
#include "li_die_area.h"
#include "li_highlight.h"
#include "li_instance.h"
#include "li_layer.h"
#include "li_manager.h"

namespace open_edi {
namespace gui {

#define USE_MUTI_THREAD 1

LI_Manager::LI_Manager() {
}

void LI_Manager::preDrawAllItems() {

    //refresh all routing layers
    for (auto layer : getRoutingLayersList()) {
        layer->refreshBoundSize();
        layer->refreshItemMap();
    }

    //draw all items
    for (auto item : li_list_) {
        if (item->isMainLI()) {
#if USE_MUTI_THREAD == 1
            QThreadPool::globalInstance()->start(item);
#else
            item->preDraw();
#endif
        }
    }
#if USE_MUTI_THREAD == 1
    QThreadPool::globalInstance()->waitForDone();
#endif
}

QList<LI_Base*> LI_Manager::getLiList() {
    return li_list_;
}

void LI_Manager::setLiVisibleByName(QString name, bool v) {

    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setVisible(v);
    }
}

void LI_Manager::setLiBrushStyleByName(QString name, Qt::BrushStyle brush_style) {
    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setBrushStyle(brush_style);
    }
}

LI_Base* LI_Manager::getLiByName(QString name) {
    if (li_map_.find(name) != li_map_.end()) {
        return li_map_[name];
    }
    return nullptr;
}

void LI_Manager::addLayer(open_edi::db::Layer* layer) {
    auto li_layer = new LI_Layer(layer);
    createLI(li_layer);
    if (layer->isRouting()) {
        routing_layer_list_.append(li_layer);
    }
}

void LI_Manager::setLayerVisibleByName(QString name, bool v) {
}

void LI_Manager::setLayerColorByName(QString name, QColor color) {
}

LI_Base* LI_Manager::getLayerByName(QString name) {
    auto li = getLiByName(name);
    if (li->type() == LI_Base::ObjType::kLayer) {
        return li;
    }
    return nullptr;
}

QList<LI_Layer*> LI_Manager::getLayerList() {
    QList<LI_Layer*> list;
    for (auto li : li_list_) {
        if (li->type() == LI_Base::ObjType::kLayer) {
            list.append(dynamic_cast<LI_Layer*>(li));
        }
    }

    return list;
}

/**
 * LI_Manager 
 * set selected area to db, save objects to each LI 
 * 
 * @param  {QPointF} p1 : db uint coordinate of low left point
 * @param  {QPointF} p2 : db uint coordinate of up right point
 */
void LI_Manager::setSelectedArea(QPointF p1, QPointF p2) {

    open_edi::db::Box area(p1.rx(), p1.ry(), p2.rx(), p2.ry());

    setSelectedArea(area);
}

void LI_Manager::setSelectedArea(int x, int y, int w, int h) {

    open_edi::db::Box area(x, y, x + w, y + h);

    setSelectedArea(area);
}

void LI_Manager::setSelectedArea(open_edi::db::Box area) {

    selected_box_ = area;

    for (auto item : li_list_) {
        if (item->isMainLI()) {
            if (item->isHighlightLI()) {
                auto li_highlight = dynamic_cast<LI_HighLight*>(item);
                li_highlight->setOffsetX(area.getLLX());
                li_highlight->setOffsetY(area.getLLY());
            } else {
                item->setSelectedArea(area);
            }
        }
    }
}

void LI_Manager::setHighlightSelectionArea(open_edi::db::Box area) {
    auto li_highlight = dynamic_cast<LI_HighLight*>(getHighlightLI());
    li_highlight->setOffsetX(getAnchorLLX());
    li_highlight->setOffsetY(getAnchorLLY());
    li_highlight->setSelectedArea(area);
    li_highlight->preDraw();
    li_highlight->update();
}

void LI_Manager::resetSelectedBox() {
    selected_box_.setLLX(0);
    selected_box_.setLLY(0);
    auto max_width = dynamic_cast<LI_DieArea*>(getLiByName(kLiDieAreaName))->getDieW();
    selected_box_.setURX(max_width);
    auto max_height = dynamic_cast<LI_DieArea*>(getLiByName(kLiDieAreaName))->getDieH();
    selected_box_.setURY(max_height);
}

LI_Base* LI_Manager::getHighlightLI() {
    return getLiByName(kLiHighlightName);
}

LI_Manager* LI_Manager::inst_ = nullptr;
} // namespace gui
} // namespace open_edi