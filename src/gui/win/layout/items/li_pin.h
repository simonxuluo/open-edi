#ifndef EDI_GUI_LI_PINS_H_
#define EDI_GUI_LI_PINS_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_pin.h"
#include "../graphics_scene.h"
#include "../util/transform.h"
#include "li_base.h"

namespace open_edi {
namespace gui {

class LI_Layer;

class LI_Pin : public LI_Base {
  public:
    explicit LI_Pin(ScaleFactor* scale_factor);
    LI_Pin(const LI_Pin& other) = delete;
    LI_Pin& operator=(const LI_Pin& rhs) = delete;
    ~LI_Pin();

    virtual void preDraw() override;
    LGI_Pin*     getGraphicItem();
    void         drawPins(open_edi::db::Inst& ins, Transform& transform);
    virtual bool isMainLI() { return true; };
    LI_Layer*    getLayer(open_edi::db::Pin* pin);
    LI_Layer*    getBindingLayer() { return binding_layer_; };
    virtual void setVisible(bool visible);
    virtual void setZ(int z) override;

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Pin*  item_{nullptr};
    LI_Layer* binding_layer_{nullptr};
};
} // namespace gui
} // namespace open_edi

#endif
