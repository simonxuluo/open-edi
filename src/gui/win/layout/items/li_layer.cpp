#include "li_layer.h"

#include "../layout_def.h"
namespace open_edi {
namespace gui {

LI_Layer::LI_Layer(open_edi::db::Layer* layer) : LI_Base() {
    item_ = new LGI_Layer;
    item_->setLiBase(this);
    type_      = ObjType::kLayer;
    visible_  = true;
    db_layer_ = layer;
    name_     = layer->getName();
    setZ(layer->getZ());
    // li_mgr_->addLI(this);
    item_->setVisible(visible_);
}

LI_Layer::~LI_Layer() {
}

void LI_Layer::preDraw() {
    item_->setVisible(visible_);
    if (!visible_) {
        return;
    }
}

bool LI_Layer::hasSubLI() {
    return false;
}

LGI_Layer* LI_Layer::getGraphicItem() {
    return item_;
}

bool LI_Layer::isMainLI() {
    return true;
}

void LI_Layer::setZ(int z) {
    z_ = z + (int)(LayerZ::kRouting);
    item_->setZValue(z_);
}

} // namespace gui
} // namespace open_edi