#include "li_pin.h"

#include <QDebug>

#include "../layout_def.h"
#include "li_layer.h"

namespace open_edi {
namespace gui {
LI_Pin::LI_Pin(ScaleFactor* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Pin;
    item_->setLiBase(this);
    pen_.setColor(QColor(0, 0, 0xff, 0xff));
    brush_ = QBrush(QColor(0, 0, 0xff, 0xff), Qt::Dense5Pattern);
    type_   = ObjType::kPin;
    name_  = kLiPinName;
    //defualt visible
    visible_ = true;
    setZ(0);
}

LI_Pin::~LI_Pin() {
}

LGI_Pin* LI_Pin::getGraphicItem() {
    return item_;
}

void LI_Pin::draw(QPainter* painter) {
    LI_Base::draw(painter);
}

void LI_Pin::drawPins(open_edi::db::Inst& ins, Transform& transform) {
    if (!visible_) {
        return;
    }

    auto box = ins.getBox();

    auto factor = *scale_factor_;

    auto li_inst_box = transform.translateDbBoxToQtBox(box, getSceneHight());

    // Todo!!
    if (!(li_inst_box.getWidth() > 4) && li_inst_box.getHeight() > 4) {
        return;
    }

    if (ins.numPins()) {
        // printf("instance number of pins %d\n", ins.numPins());

        std::vector<open_edi::db::Box> box_vector;

        auto pins        = ins.getPins();
        auto location    = ins.getLocation();
        auto pins_vector = open_edi::db::Object::addr<open_edi::db::ArrayObject<open_edi::db::ObjectId>>(pins);
        auto factor      = *scale_factor_;

        for (auto iter = pins_vector->begin(); iter != pins_vector->end(); ++iter) {
            auto pin = open_edi::db::Object::addr<open_edi::db::Pin>(*iter);
            if (!pin) {
                continue;
            }

            //transform and filp according instance orient
            pin->getBoxVector(box_vector);

            if (!binding_layer_) {
                binding_layer_ = getLayer(pin);
            }

            if (!binding_layer_->isVisible()) {
                return;
            }

            // qDebug()<<binding_layer_->getGraphicItem()->zValue();
            // binding_layer_->getGraphicItem()->setZValue(0);

            QPainter painter(binding_layer_->getGraphicItem()->getMap());

            // QPainter painter(getGraphicItem()->getMap());

            pen_.setColor(binding_layer_->getColor());

            painter.setPen(pen_);

            brush_.setColor(pen_.color());

            int pin_llx, pin_lly, pin_urx, pin_ury;

            for (int index = 0; index < box_vector.size(); ++index) {

                if (!(box_vector[index].getWidth() >= 1 || box_vector[index].getHeight() >= 1)) continue;

                auto li_pin_box = transform.translateDbBoxToQtBox(box_vector[index], getSceneHight());

                pin_llx     = li_pin_box.getLLX();
                pin_lly     = li_pin_box.getLLY();
                pin_urx     = li_pin_box.getURX();
                pin_ury     = li_pin_box.getURY();
                auto width  = li_pin_box.getWidth();
                auto height = li_pin_box.getHeight();

                // painter.setPen(pen_);

                //draw inside
                painter.fillRect(QRectF(pin_llx,
                                        pin_ury,
                                        width,
                                        height),
                                 brush_);

                //draw outline
                painter.drawRect(pin_llx, pin_ury, width, height);
            }

            box_vector.clear();
        }
    }
}

LI_Layer* LI_Pin::getLayer(open_edi::db::Pin* pin) {
    auto term = pin->getTerm();
    for (int i = 0; i < term->getPortNum(); i++) {
        auto port = term->getPort(i);
        for (int j = 0; j < port->getLayerGeometryNum(); j++) {
            auto geo      = port->getLayerGeometry(j);
            auto li_layer = dynamic_cast<LI_Layer*>(li_mgr_->getLayerByName(geo->getLayer()->getName()));
            return li_layer;
        }
    }
    return nullptr;
}

void LI_Pin::setVisible(bool visible) {
    visible_ = visible;
    getGraphicItem()->setVisible(visible_);

    // if (binding_layer_ && (!visible_)) {
    //     binding_layer_->refreshBoundSize();
    //     binding_layer_->refreshItemMap();
    //     binding_layer_->update();
    // }
}

void LI_Pin::setZ(int z) {
    z_ = z + (int)(LayerZ::kRouting);
    item_->setZValue(z_);
}

void LI_Pin::preDraw() {
    if (!visible_) {
        return;
    }
    refreshBoundSize();
    refreshItemMap();
    std::vector<open_edi::db::Object*> result;
    open_edi::db::fetchDB(selected_area_, &result);

    auto factor = *scale_factor_;

    Transform transform(factor, factor, -selected_area_.getLLX(), -selected_area_.getLLY());

    for (auto obj : result) {
        auto obj_type = obj->getObjectType();

        switch (obj_type) {
        case open_edi::db::ObjectType::kObjectTypeInst:
            drawPins(*static_cast<open_edi::db::Inst*>(obj), transform);
            break;
        case open_edi::db::ObjectType::kObjectTypePin:
            break;

        default:
            break;
        }
    }
}

} // namespace gui
} // namespace open_edi