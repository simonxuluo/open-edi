#ifndef OPEN_EDI_LAYOUT_TRANSFORM_H_
#define OPEN_EDI_LAYOUT_TRANSFORM_H_

#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "util/util.h"
#include "../layout_def.h"

#include <qmath.h>
#include <QDebug>

namespace open_edi {
namespace gui {

class QtBox {
  public:
    auto getLLX() { return llx; };
    auto getLLY() { return lly; };
    auto getURX() { return urx; };
    auto getURY() { return ury; };
    auto getWidth() { return w; };
    auto getHeight() { return h; };
    auto getLUX() { return lux; };
    auto getLUY() { return luy; };
    auto getRLX() { return rlx; };
    auto getRLY() { return rly; };

    int llx{0};
    int lly{0};
    int urx{0};
    int ury{0};
    int w{0};
    int h{0};

    int lux{0};
    int luy{0};
    int rlx{0};
    int rly{0};

    ~QtBox(){};
};

class Transform {

  public:
    Transform(){};
    Transform(ScaleFactor sx, ScaleFactor sy, int offset_x = 0, int offset_y = 0);
    ~Transform();

    template <typename T>
    auto translateToLiBox(T&& db_box) {
        open_edi::db::Box li_box;
        li_box.setLLX(qFloor((db_box.getLLX() + offset_x_) * scale_x_));
        li_box.setLLY(qFloor((db_box.getLLY() + offset_y_) * scale_y_));
        li_box.setURX(qFloor((db_box.getURX() + offset_x_) * scale_x_));
        li_box.setURY(qFloor((db_box.getURY() + offset_y_) * scale_y_));
        return std::move(li_box);
    }
    // open_edi::db::Box&& translateToLiBox(open_edi::db::Box&& box);
    int  translateX(int x);
    int  translateY(int y);
    void setOffsetX(int offset_x) { offset_x_ = offset_x; };
    void setOffsetY(int offset_y) { offset_y_ = offset_y; };
    void setScale(ScaleFactor sx, ScaleFactor sy) { scale_x_ = sx, scale_y_ = sy; };
    int  translateDbYToQtY(int db_y, int sceen_h) { return sceen_h - db_y; };

    template <typename T>
    auto translateDbBoxToQtBox(T&& db_box, int sceen_h) {

        auto li_box = translateToLiBox(db_box);

        // qDebug()<<li_box.getURY();

        QtBox box;
        box.llx = li_box.getLLX();
        box.lly = translateDbYToQtY(li_box.getLLY(), sceen_h);
        box.urx = li_box.getURX();
        box.ury = translateDbYToQtY(li_box.getURY(), sceen_h);
        box.w   = li_box.getWidth() ? li_box.getWidth() - 1 : 0;
        box.h   = li_box.getHeight() ? li_box.getHeight() - 1 : 0;

        box.lux = box.llx;
        box.luy = box.ury;
        box.rlx = box.urx;
        box.rly = box.lly;

        return std::move(box);
    };

  private:
    ScaleFactor scale_x_{1};
    ScaleFactor scale_y_{1};
    int   offset_x_{0};
    int   offset_y_{0};
};

} // namespace gui
} // namespace open_edi
#endif
