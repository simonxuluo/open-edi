#include "transform.h"

namespace open_edi {
namespace gui {
Transform::Transform(ScaleFactor sx, ScaleFactor sy, int offset_x, int offset_y) {
    scale_x_  = sx;
    scale_y_  = sy;
    offset_x_ = offset_x;
    offset_y_ = offset_y;
}

Transform::~Transform() {
}

int Transform::translateX(int x) {
    return x * scale_x_ + offset_x_;
}

int Transform::translateY(int y) {
    return y * scale_y_ + offset_y_;
}


} // namespace gui
} // namespace open_edi
