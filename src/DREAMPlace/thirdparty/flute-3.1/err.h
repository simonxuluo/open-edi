#ifndef _ERR_H_
#define _ERR_H_

void  err_msg(
const char* msg
);

void  err_exit(
const char* msg
);

#endif
